// Lokalizacje plików
val songsFile = "/home/dawid/PMD/lab1/songs"
val listenFile = "/home/dawid/PMD/lab1/listen"
val usersFile = "/home/dawid/PMD/lab1/users"
val datesFile = "/home/dawid/PMD/lab1/dates"

// 1. Ranking popularności utworów

// Ładowanie danych
val songs = spark.read.
	option("delimiter", ",").
	csv(songsFile).
	toDF("song_id", "track_long_id", "song_long_id", "artist", "song")
val listen = spark.read.
	option("delimiter", ",").
	csv(listenFile).
	toDF("old_song_id", "song_id", "date_id", "user_id")
//

listen.
	groupBy("song_id").
	count.
	join(songs, listen("song_id") === songs("song_id")).
	select("song", "count").
	orderBy(desc("count")).
	show()


// 2. Ranking użytkowników ze względu na największą liczbę odsłuchanych unikalnych utworów

// Ładowanie danych o użytkownikach
val users = spark.read.
option("delimiter", ",").
csv(usersFile).
toDF("user_id", "long_user_id")
//

listen.
	groupBy("user_id").
	agg(countDistinct("song_id").alias("count")).
	join(users, listen("user_id") === users("user_id")).
	select("long_user_id", "count").
	orderBy(desc("count")).
	show(10)


// 3. Artysta z największą liczbą odsłuchań 
     
listen.
	groupBy("song_id").
	agg(count("*").alias("count")).
	join(songs, listen("song_id") === songs("song_id")).
	select("artist", "count", "song").
	groupBy("artist").
	agg(sum("count").alias("count")).
	select("artist", "count").
	orderBy(desc("count")).
	show(1)


// 4. Sumaryczna liczba odsłuchań w podziale na poszczególne miesiące

// Ładowanie danych o datach
val dates = spark.read.
	option("delimiter",",").
	csv(datesFile).
	toDF("date_id", "timestamp", "year", "month", "day")
//

val listenPerMonthQuery = listen.
	groupBy("date_id").
	agg(count("*").alias("count")).
	join(dates, listen("date_id") === dates("date_id")).
	select("count", "month").
	groupBy("month").
	agg(sum("count").alias("month_count")).
	select("month", "month_count").
	orderBy(asc("month")).
	show(12)

// 5. Wszyscy użytkownicy, którzy odsłuchali wszystkie trzy najbardziej popularne piosenki zespołu Queen

val mostPopularQueenSongsQuery = listen.as("listen").
	groupBy("song_id").
	agg(count("*").alias("count")).
	join(songs.as("songs"), $"listen.song_id" === $"songs.song_id").
	filter(col("artist") === "Queen").
	select($"listen.song_id").
	orderBy(desc("count")).
	limit(3)


listen.as("listen").
	select($"listen.song_id", $"listen.user_id").
	join(mostPopularQueenSongsQuery.as("m"), $"listen.song_id" === $"m.song_id").
	groupBy($"listen.user_id", $"m.song_id").
 	count().
	select("user_id").
	groupBy("user_id").
	count().
	filter(col("count") === 3).
	orderBy(asc("user_id")).
	select("user_id").as("q").
	join(users.as("u"), $"q.user_id" === $"u.user_id").
	select($"u.long_user_id").
	show(50)
