import org.apache.spark.SparkConf
import org.apache.spark._
import org.apache.spark.SparkContext._

val mFile = "/home/dawid/PMD/lab2/M.txt"
val nFile = "/home/dawid/PMD/lab2/N.txt"

val m = sc.
	textFile(mFile).
	map(line =>{
		val t = line.split(" ");
		(t(1).trim.toInt, (t(0).trim.toInt, t(2).trim.toInt))
    })

val n = sc.
	textFile(nFile).
	map(line =>{
		val t = line.split(" ");
		(t(0).trim.toInt, (t(1).trim.toInt, t(2).trim.toInt))
	})

m.
	join(n).
	map{ case(j, ((i,v),(k,w))) => ((i,k), v * w)}.
	reduceByKey(_ + _).
	collect
