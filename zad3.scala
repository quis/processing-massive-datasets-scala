import  scala.util.Random
import org.apache.spark._
import org.apache.spark.SparkContext._

val m = 10

val groups = Map(
    ( for  ( i <-0  to m)  yield  { 
		val mu = Random.nextDouble()*10-5
		val  std = Random.nextDouble()*10-5
		( i -> (mu,std ))
	}): _*)

val n = 10

val data = for ( i <- 1 to n) yield { 
	val  g = random.nextInt(m);
	val  (mu,  sigma) = groups (g);	(g, mu + sigma*random.nextGaussian () )
}

val dataRDD = sc.parallelize(data)

val countPerGroup = dataRDD.map{case (x,y) => (x,1)}.reduceByKey( _ + _ )
var countAll = countPerGroup.map{case(x,y) => (y)}.reduce(_ + _)

val averagePerGroup = dataRDD.
	mapValues( v => (v, 1)).
	reduceByKey( (x,y) => (x._1 + y._1, x._2 + y._2) ).
	mapValues( x => (x._1.toDouble / x._2.toDouble) )

val averageAll = averagePerGroup.
	map{case(x,y) => (1,y)}.
	mapValues(v => (v, 1)).
	reduceByKey((x,y) => (x._1 + y._1, x._2 + y._2)).
		mapValues(x => (x._1.toDouble / x._2.toDouble)).
		map{case(x,y) => (y)}.reduce(_ + _)

val variancePerGroup = dataRDD.mapValues( v => (1,v,v*v)).
	reduceByKey((x,y) => (x._1 + y._1, x._2 + y._2, x._3 + y._3)).
	mapValues(x => (x._3/x._1) - ((x._2/x._1)*(x._2/x._1)))

val varianceAll = dataRDD.map{case(x,y) => (1,y)}.
	mapValues( v => (1,v,v*v)).
	reduceByKey( (x,y) => (x._1 + y._1, x._2 + y._2, x._3 + y._3) ).
	mapValues( x=> (x._3/x._1) - ((x._2/x._1)*(x._2/x._1))).
	map{case(x,y) => (y)}.reduce(_ + _)

countPerGroup.take(10)
countAll
averagePerGroup.take(10)
averageAll
variancePerGroup.take(10)
varianceAll

